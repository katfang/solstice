var express = require('express');
var router = express.Router();
var utildata = require('../data/utildata.json');

/*
GET utildata page.
This is read globally larger because it's easy (and certainly doesn't need to
be recreated each time the server hits this page ... which I'm currently sort
of just assuming is what happens here by putting it in global scope rather than
in the get method.
*/
router.get('/', function(req, res, next) {
  res.json(utildata); 
});

module.exports = router;
