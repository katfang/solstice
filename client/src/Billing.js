import React, { Component } from 'react';
import BarChart from './BarChart';
import * as utildata from './utildata.js';

class Billing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    utildata.fetchAsc().then(data => {
      this.setState({data: data});
    });
  }

  render() {
    return (
      <div>
        <h2>Billing $$ per month</h2>
        <BarChart
          id='billing'
          data={this.state.data}
          value={(d) => d['bill']}
          datelabel={(d) => d.month + '/' + d.year}
          valuelabel={(d) => '$' + d.bill}
          width='700'
          height='400'/>
      </div>
    )
  }
}

export default Billing;
