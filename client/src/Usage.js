import React, { Component } from 'react';
import BarChart from './BarChart';
import * as utildata from './utildata.js';

class Usage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  componentDidMount() {
    utildata.fetchAsc().then(data => {
      this.setState({data: data});
    });
  }

  render() {
    return (
      <div>
        <h2>Usage kWh used per month</h2>
        <BarChart
          id='utildata'
          data={this.state.data}
          value={(d) => d.kwh/5}
          datelabel={(d) => d.month + '/' + d.year}
          valuelabel={(d) => d.kwh}
          width='700'
          height='400'/>
      </div>
    )
  }
}

export default Usage;
