import React, { Component } from 'react';
import * as d3 from 'd3';

const BAR_WIDTH = 100;
const BAR_MARGIN = 5;
const DATE_LABEL_MARGIN = 40; 

class BarChart extends Component {

  componentDidMount() {
    this.drawChart();
  }

  componentDidUpdate() {
    this.drawChart();
  }

  drawChart() {
    // TODO: do scaling better

    // show bars
    this.svg.selectAll('rect')
      .data(this.props.data)
      .enter()
      .append('rect')
      .attr('x', (d, i) => i * (BAR_WIDTH + BAR_MARGIN))
      .attr('y', (d, i) =>
        this.props.height - this.props.value(d) - DATE_LABEL_MARGIN)
      .attr('width', BAR_WIDTH)
      .attr('height', (d, i) => this.props.value(d))
      .attr('fill', 'green');

    // TODO: Add axes, but for now, have labels for at least understanding.
    // Date label at the bottom of the chart 
    this.svg.selectAll('text.datelabel')
      .data(this.props.data)
      .enter()
      .append('text')
      .text((d) => this.props.datelabel(d))
      .attr('x', (d, i) => i * (BAR_WIDTH + BAR_MARGIN))
      .attr('y',  (d, i) => this.props.height - 20)

    // Value label right above the bar (... since there's no y-axis or hover yet) 
    this.svg.selectAll('text.valuelabel')
      .data(this.props.data)
      .enter()
      .append('text')
      .text((d) => this.props.valuelabel(d))
      .attr('x', (d, i) => i * (BAR_WIDTH + BAR_MARGIN))
      .attr('y',  (d, i) =>
        this.props.height - this.props.value(d) - 3 - DATE_LABEL_MARGIN);
  }

  render() {
    return (
      <svg
        width={this.props.width}
        height={this.props.height}
        ref={element => (this.svg = d3.select(element))}>
      </svg>
    );
  }
}

export default BarChart;
