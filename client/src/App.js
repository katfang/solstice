import React, { Component } from 'react';
import {
  Route,
  NavLink,
  HashRouter
} from 'react-router-dom';
import Home from './Home';
import Usage from './Usage';
import Billing from './Billing';

class App extends Component {
  render() {
    return (
      <HashRouter>
        <div>
          <h1>Utility Data</h1>
          <ul>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/usage">Usage</NavLink></li>
            <li><NavLink to="/billing">Billing</NavLink></li>
          </ul>

          <div className="content">
            <Route exact path="/" component={Home} />
            <Route path="/usage" component={Usage} />
            <Route path="/billing" component={Billing} />
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
