exports.fetch = function() {
  return fetch('/utildata')
    .then(results => {
      return results.json();
    });
}

exports.sortYearMonthAsc = function(first, second) {
  // January is 0 for some reason
  return new Date(first.year, first.month-1).getTime()
    - new Date(second.year, second.month-1).getTime();
}

exports.fetchAsc = function() {
  return exports.fetch().then(data => {
    data.sort(exports.sortYearMonthAsc);
    return data;
  });
}
