import React, { Component } from 'react';

class Home extends Component {
  render() {
    return (
      <div>
        <h2>Home</h2>
        <p>
          Here you might expect some text explaining your latest bill or new
          developments for your power provider.
        </p>
      </div>
    )
  }
}

export default Home;
